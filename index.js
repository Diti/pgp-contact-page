const STR_ENCRYPT = "🔓 Encrypt";
const STR_SEND = "🔒 Encrypted!";

var keyManager;

var fieldMessage = document.getElementById('message');
var btnSubmit = document.getElementById('submit-btn');

window.addEventListener("load", function(e) {
    // Hint the user that the message will get encrypted
    btnSubmit.innerHTML = STR_ENCRYPT;

    loadPublicKey();
});

document.getElementById("contact-form").addEventListener("submit", function(e) {
    // Prevent the form from submitting itself
    e.preventDefault();

    // Encrypt the message, unless it's been done already
    if (fieldMessage.value.substring(0, 27) != "-----BEGIN PGP MESSAGE-----") {
      encryptMsg(keyManager);
    } else {
      btnSubmit.innerHTML = STR_SEND;
    }

    //this.submit();
});

function loadPublicKey() {
  // Import my public key, contained in const `diti_pubkey`
  const diti_pubkey = "-----BEGIN PGP PUBLIC KEY BLOCK-----\n\n" +
    "mQINBFGShUkBEACyy06H6q/eRhA7xywR+EQzWrJfM6UbNAURzY2kbUzzUANXj+fa\n" +
    "bhNdBpbI9I3yvjrcQIlJqm4mGqKnQKAT9ooCagRpZZaFIvpVRxMSYx/PE8JX5+CH\n" +
    "4AcopUSIaM2tz6+vqJhKfGcpC2+Gx/DBYxRd4Y2lGd3IUZBoTgPLDjadZidXVUzj\n" +
    "1RQRO+6j0kiewnzdPBVoqabx8oqDANv19S99P/dibA5vMp4uCaAK5KnACPJc4v3d\n" +
    "leNvbmo4QC+uk0t5NFyerR20XpQHYHxiPKqyeLsqtUUAvMGDg4YHx8YO0eIthuaN\n" +
    "DIJSW7wA1vOq32yjH4RiX2RXgP9mJ1mx+KVJA7l9gDu0nrCPaEIRyzqeQDRLbndo\n" +
    "1RI6bq5BB6HnipfWMmgD8Ci7Cl2TMZ+D8Kr1fkJ7/gQqzd7QY5Ck2y5TzdbRJUkU\n" +
    "hwtqKqI3nFjRLjdbVsjR8RZxoih6F3uXGMHAfmHIPs1XFBZbIwFxqqvNRrBzP6A3\n" +
    "TiUQ+VbaKff75xU8+pedAih9OplReOtWt8RKMImxhxwhR354kST05e2AcP6VDjjH\n" +
    "gD6RQyi8yP8Hkkr58aOJ1SNwJ7OvWbbEEP6z1SsL3N4ugJRUP+r9Ef4CxObtCJXj\n" +
    "FlgNTymRvpkXbmlip0LEOohjXXbbxNA1PcgiieHdFbzq8SQIsC2VkFetGwARAQAB\n" +
    "tDNEaW1pdHJpIFRvcnRlcmF0IChib3JuIDE5OTAtMDUtMjYgaW4gTWVsdW4sIEZy\n" +
    "YW5jZSmJAjoEEwEIACQCGwECHgECF4ACGQEFAlMXFhsFCwkIBwMFFQoJCAsFFgID\n" +
    "AQAACgkQMaSRIc1C/wCN/w//X9cF1j2drEWzLWOamnjFQoD52muCD82ewD22Er9d\n" +
    "Mk8HCpyKF1yMVM+/aT73nszoH3O6xeOJ5M5A/TV4o/RABnyiPBk4jqX/WgylI8xd\n" +
    "SkZL2afNanay3gMf/5ssEiPtm0GjdEp2q9ZhZUF4WoTcNGhJaAaKBWTHAsiZtWtK\n" +
    "wtf2xFYhsU74s5kYh3BQQgk5quR4zCxBYeAQLGSSVba7Ih7g967+gS/K3FhjZZdN\n" +
    "t11L+bBH6XSAzZeqinZfmfu5GM56wFq+KcLdueLhgmY3Vtl+BlOMvB0cLQpqVEB1\n" +
    "FFiEqR29fYWXVsm47+rIgRXYc9XLV/1hDyfKoI/+XwEjQDAcWoA0Ym1G7jDtZune\n" +
    "2IRhDUclwtn9s/b4R3XukPw5q0ipLfx74q4I3zwxVtyjE7SI+AJ/tYbx1OGzWUSU\n" +
    "+puW5EHS97SGvuaPTycbkmGaXgXRHQPoqybsJ/Ftyugjd1u0+WeiQsqidHfftOMW\n" +
    "CiVlrQ4JGsGnHIdvUIfLXSHzjhP5ynu+EzG405gehG/Bfu2P5uY2kR28TjNGAKlL\n" +
    "HleQP8xq1+vqWohKooN3sG2jSR4sPeALsM4ZHw+mZVBgWyMln57Ge+PHbFX9bxoG\n" +
    "OLfIJV4xRP8hTuhFOUUjmlAi50qxP69KJjIq74nSn/IZkdgCsNyT6/O4uBNAfOtH\n" +
    "8pa0HkRpbWl0cmkgVG9ydGVyYXQgPGtyYUBkaXRpLm1lPokCNwQTAQIAIQIbAQIe\n" +
    "AQIXgAUCUxcWIQULCQgHAwUVCgkICwUWAgMBAAAKCRAxpJEhzUL/ADeiD/9vjnRH\n" +
    "Yk3+3Y+FM+QEZXgz/vAGJLSo1Nh+SP0V2jyApF//Up9+KGRStH9BHldsRS03E5sG\n" +
    "OMuuUfqGivTeETJmikliN7rZehxyC/LaB1ZOFFiN/6rGu+QBeEpv8drq4PQ53QiQ\n" +
    "It03WiH4Ee3fhpii5BRjXaKKBACxym3mNjqOalljh2dA0MzK4nqkQiCeLWlCt6g3\n" +
    "uDbzsnkx91WslxVv37zV2M/PQPVHBeRQdqbpaPNeKNP+GMsD1DAx4MAR+6gowG2E\n" +
    "to7o4sBF5y2pTPjzXD5SF2m6f+iGkzGg0sjmqBoSC7hqlJdJW4EQ4/ZqFZEbKLhz\n" +
    "oU/efyPdk8tDz2PeoH1TEt754EhV0gVsQ/SaZUtYPiMsjtRSg8x7kwzI4wTgoSJE\n" +
    "5um40ScRacbDfKnNNHmfF4t7CfKarWb7pPVFoYzFvP5xMxbuQFbYN2brHa3vjCMz\n" +
    "av06N/vxzbgs1uJtlUe3w7aWJkQOKj1bDfDgUITXh8yR+bTRqAdfWE/jwqRzUjgn\n" +
    "wNQevzslRPcgv/itXvnxkyc+ESMscpmaS8PJyWf4bqrwY+d8SIUBf4jif/6vaJVT\n" +
    "ICSU7NXbocdwTjkzVycy2ZpCoIaGsdReAm95nYQDD+4YDq2XcxELrtizEy1/wa4v\n" +
    "puIvroKar6hU0K3R/FhsBoWLrgtFIbL4yPKnw7kCDQRRko7xARAA0Ze8FRve42fJ\n" +
    "vS5gIqpxhWhbSaXY5h4OOIkeR08jLtiNzRWU8bRafdlurHlr1N9eitC+skrIvFUx\n" +
    "NuJ2TOtOuDAqZ7zTnPTg6C/2/Cp+y3scXrqAOe7eYlaY/NavouB1QWVGgR/tohqZ\n" +
    "+7DhZHymXqesTBVm2UTnLrX2O6BgLnpcp0cJS1xt08C8DYLvN3Y8+S+9Nmzu4En4\n" +
    "DkPOrGczuW/XF9EuERS29lqJhM1LqfCxpyVETLAq+tAQ2uB6euNjCr4HkMq9NpWT\n" +
    "wxqzPXzpo6eHY7qCf4PJfTLE0Nc1+XuT0WXxiO6YRijcIVMKHqEe52giGqo/LQ4w\n" +
    "MoGlyOM/GInhZQ5dhie1Gm4l2QRkgpTbWP6AQdqbFdn0Lt2YvsVWGgniejvckAge\n" +
    "9MQTFOv9dGCFmfKErz5Jm2tcPzt/y6aIVsg5nfsV9qqP6BXrO2fKrrje6uYXqtpP\n" +
    "BBARx/Eif/L9MvZws2CFMY0V+y8d8N5b6vHj72UI2VWXk+lMaVdw10uckk+Nnosg\n" +
    "iOAtJvVcVAl6qhosh5gQ3SgEMCPHVpNqoGdQKRPdV91rzt4kbfN3CUzKxD/WUDpw\n" +
    "BWHmxH7HJ6su/QDNomD4qVtQ6nFNMKC4ObivmXD2YFJwSFrzMgHzZOImXQZqpns3\n" +
    "+bmMIsRcMaymJcGIb2SmmGILjJja7PEAEQEAAYkEPgQYAQIACQUCUZKO8QIbAgIp\n" +
    "CRAxpJEhzUL/AMFdIAQZAQIABgUCUZKO8QAKCRBup+yIDHporr72D/47wnPrBaPq\n" +
    "33KDpqjttGjdg73THu4x6cuiGQxYz8HfkYWCzgBBdEzxW1FvRj2pGFt12iUBuaod\n" +
    "mOyoe4ynl/Tedso06Ssu0Y+kUcGFgaungZ/1sxQrvQccjhMGEWDaIGF73Jqc5XQC\n" +
    "WGziiBHXIBne9CSDOzlg6NUeDRFAwdCN4bAQlef/l673W6LPUw8rrZSdM8kk+f8N\n" +
    "Zq9v5dPuMGesN37QH88+Pwy+yQYjTXRLFgPurryudhqBhQ0jnuIGcLkIgAYnlAg7\n" +
    "JIt2NtCJQN34znlpCvZ7w8cXFEkucH8TOxmVVCC64WppcladmZHqO5dMM9WNga0O\n" +
    "1wiNpFKcPQV73JPihLfXn6TelfVAByXcWvXlJl/FDV687qH7Pg7aN1NAcGFcTfzg\n" +
    "Ef/9vzmFyyo0Zr1IvgH8TxCiR0nZxvdrvrr9AzRnI0QyNfnZMFoLYMh4jc4f5ryn\n" +
    "fZTL4WVfIhlrOUE2IDjY/nfVwQ1Ejl9bMVKgl705klQR1oeEEoph4syz4Q1AkDVQ\n" +
    "Es7cjAcSfdsqNevpLporcDEdU7aSMPYXgmvdj8LKF3SD9SPXwUupTvfcZgJ5Dpsd\n" +
    "t735sph9m2CWFkr0PqQzsfW8DR59hFFh1othRWVZBhz7U+Dxe6QdFE3a/RpDB13t\n" +
    "FXnGgEh9J6vaNGrm6QKQBbU6Ue5hwM9G/PcMD/9ZMBLvMJ4oFrzo/lZfTmaCMKAC\n" +
    "kJj+PaXYQLLXzCTG5cJSIYLXjU+GNVtku7IXCJ4GkR7r01KZBNj+zkXy1IE5Ch7v\n" +
    "JADTR81NxxBD3eQ9ijjpEaSc4Y+mXv3SpjWWK1lzidekaJu2nu70BWD399NyzjQI\n" +
    "Gi+i6WaYoqZuK5+OY0qI1avwz1tIZkiJZmabgPkDMtAtiWnkI65cpQJd8x98/e4y\n" +
    "RMkc2R1poMAD0HWbPO+t9qBZ6Fx/SflwwScG/cjIPK5RGoSksoSAiJrDS0R/4A6Y\n" +
    "WHAGVFtVFYmcadcvjMsPRvyv4HeZiGcHprK66unzzq2KKxeljDSY2RqM+d0DZnp3\n" +
    "8kVF+UteU73r9LwOUINBeHMiAQuQQoojN/zdgtRdgtM8IHcH98U657Fetv0ROQ9O\n" +
    "bQjx1LOY7P3rbZtBT8SjwRXpIY+lpbf6w9c60yY2bAsYqjB7i3opkuGQ8k6MyJ5x\n" +
    "IRlG5Kf9b8oca6FKO5fG8V3uMwj1yeTzmU70VdZ35sbpcGOooxG6ER+5cGS9sb6T\n" +
    "OsB/BylaQKIgNH2PhMEUHUbxjAzVdUBH0vJfW8LV1g26JJ4Av0XMoxAhfOE7oU+H\n" +
    "fQIiJKDiOs7aXgp4DOe39JBcfFzBzmoz60iXZn1cYH0vmC4LoJtXB3vo+cG1xc1H\n" +
    "+xXo1whN/InxLGaEV7kCDQRRkpZKARAAyf1yPtIdoHh+eWXPOjxNFQBNrqWeIhbP\n" +
    "fdyDl1v4v2uhXj1kvxwIACGBPEHBAyw496zxDSZ5AJ9dEDcnZWqQZYl6Jn09OSuT\n" +
    "MhY4UnTMutkGGqBQFWKvGr+CeswQG6jr2PotPM+s4V9UxCjLDNlkqZnGTGS4OXHZ\n" +
    "D62U4lQ0GmqKFesjcgXWhXHy5Krff4LakSQgoQXVkOTPEvfWoJYqRvKJWmqdBYdc\n" +
    "vbrB3VlXkWgXTpsxfj9XBXh5JsZr75v7zOIV/7WVWdy0I3+fOM9R6OHVh63NW8zS\n" +
    "K/FSZz2KCPmhWg+6oySbzqww6G6geBsFxayEEEcULbKjST2RIrTtDBkwYKAne1WN\n" +
    "R3LdUtd9GG4O/fZJ5MsjM1DDhMlnWu7QX3YSbpWus/WSZtPF1AzJxG1StYJ5E5dj\n" +
    "J3lccetEHOfXSQukC6UrI13LCEUZ4sDRsHqQ2lgkG1L4N8JiJ4r1dZDmYjkbsOIw\n" +
    "I+Aplo2xuR1fVX5aKD9+muwI6njn2015jY/7ByBD8eFQvbi07ZhLQYogOVRXu4QO\n" +
    "6eZhp/WFJj0+6ihUiRvmYnwLfE4hZJfZSX7uERDyTzDUNGmfzXW5RvGLorz2X7hK\n" +
    "X/YB6Ki2u6V7u1U+l95a/DaMa+EtTAS5SDiPksSSG7ozc52UzdiK3OIq5B2BQepl\n" +
    "NDJLHydOWA8AEQEAAYkCHwQYAQIACQUCUZKWSgIbDAAKCRAxpJEhzUL/AOn1D/48\n" +
    "R82cnuRt91k2ET1OCq/7R5hWnUNQzvCUMg1tcnmBGNW9zuXRCpsVI4ZabZsI+KRM\n" +
    "jCn6VQtSQp38XacW6JaX4HvQcmkQF3ddMw/2gr5q9li345BtQqdLYMGtooKxWbHH\n" +
    "feXYvkAkcI33F5OvroESlUfGcwaswBrc/BpVnHrFd/2RXiCYRMgHoBuUcaXMukUX\n" +
    "WuSGS6WsLind8Z3869ts4qY+38SQLBOO9elBOeAxRsYgpzNJK1sHq08o2jPDVNiB\n" +
    "POdtQrJ6ONmmVN9/KxGKVQ/fpi/LZHNDLKufn1fa3xvLpHPqn2LPkwusUkwaYWzD\n" +
    "Xsr4IUKszgmcR9cZr8n4srfnwrbdadL7ZbgBPJixV+ctnkUJijj2FQ8tYes13zg2\n" +
    "1/5YiM3cpop+Z9jEMTIlA7lSOQCkglhbmAGVaV3xsxbDR+yp3nyLGfumluZDxBoe\n" +
    "3Ska9KHD2PdKQizlJFi10bDERUou+nYMVfAeaoo5zONrxSB/t3MmNBqsyS/J45/f\n" +
    "B4tnHkESWdOXxXFH1Wqw10Vx2u4asv1Ew5ce2p8w5TdnaXXpWOvTzACPTjAs2G6b\n" +
    "jGxBkVk5zv5pRGYMXpSlqQTGjPpkDwFRbS5kJQIIo8o7JqSGaxmir32v2+PPsPRv\n" +
    "sXspSBXdx1rZ61zAvkvY300cHWpC3ecR5BhurUKi5bkCDQRRkqm8ARAAwrk89fJw\n" +
    "1d0TNsbFhuXvHlDFWLtMEk5Xh+6T6lhqr2L7qBJxOEghs0U4I/Rd2f9Fh6PmiPSN\n" +
    "6k9cWEw+aKLvhp//vwhsu0NnoO43sPq2+LfrQLO+CzxraJZPmA/aSKDm5fNk+bLy\n" +
    "7oeuyH0VdpYvBVd9GsySmt6cXXv9/Lm+nFwF7MK4c3k1/sekJIrFByeZzf2jVwKJ\n" +
    "XyJZl/bxB95FLMBYYjOJmx6pvzLZGxZ56y1Byq4NW6ajDEws3AHotZvstcvQStQC\n" +
    "LG17Mdz0s3x2EBA16lsY3JUjSJYEyFo3UZCWsBii2evfEChr/JedZ2FF+94UEkQb\n" +
    "jbh3AfLP0x1EJWRGTl8vstFWfONuLx9cXdQVmhC3A6UnBCTXhNJeRyDVTdHDSR4Z\n" +
    "z9Pt9c7JtJnH9tMnEAc84FMfZ8xeodFzozlF6R1WXyyMxFee4cXzU98f7j34DCCo\n" +
    "7Nm7jAnicDePpz6Vls1GSxOxMPqIXSPD84S0QlifRNSIqmVO0I1PZ3BbwSOHiiQG\n" +
    "A6z9XLq5M2mZyOO7V4GOyKP4gr2Y+JgI5Ihe3BJAfORDQGHUIFw6kO+2oyY+Y8Ra\n" +
    "X5ucCHS/aPde+DDoKSmwGnmG5Cl5IiJ0NESKCXEW5JPYGhCTz+AU9SpJf5OspCz+\n" +
    "tRYb5q1w6zfG3/SF1sPWg54kfVRm+l/jMlkAEQEAAYkCHwQYAQIACQUCUZKpvAIb\n" +
    "IAAKCRAxpJEhzUL/AEw7EACcrsxl3prY0/rSZQNcvO/EeFOk46IwTJFXXF0tUnUB\n" +
    "SdDajzWg5AR/VCqhjY8TXtQLqX51m0SWmLysrAslcv1zynriFVJum+oVKZgK2P7d\n" +
    "xC5Ix3AeQIbzv2qk7Y0rt35c7+CdbokF8+vY7+FM2pUHQOWfkB/07vCqBCWBqdQb\n" +
    "vDtar8YjBNk7VIqnhEYLig/S3zuYC1KW1hvrop7cUd+b9BkwNrR2VuIS1yBUWZnu\n" +
    "/ZSD7x8s76FooVNuErmMavesTlUvgBQ/lZBfZZvzT4xVrMnfMB4filvtnVLNxDmo\n" +
    "7lA8DfXrner2g5whm0MbesjRhcVBTMCiM+jTkWTeL9R4CKdq4Gc4/b/APzY6Cd2k\n" +
    "hCL1XmKY2vqGkY5pOVutxPI4CMfO78fjrCYYwayi2cI5wK5CXRsS+MMTNrgFD2Gw\n" +
    "B6mGS/Fdu5qQ/urG7HoYa90pekHIZYEg7BNoZMOoWNAv1Msn+pRtFyuZX5dztKXb\n" +
    "pDkQjsbuUh/AyPTWG0zIyBWG1Pil2e1Eq0lHvBT2ML8SJL/egKqYpZvwdRCjP77w\n" +
    "jK1tFDtkZj992AGZS/6J1vo35HYndLdUSgjGLQjVubIm95l+rVMDI4e7WGi/wN+x\n" +
    "IPP3eYFd2PlR27rLH75GG23WEVhftpnuGWOWpJrWpv31cQYwLLehV9SxgQgZrURO\n" +
    "dA==\n" +
    "=dGxD\n" +
    "-----END PGP PUBLIC KEY BLOCK-----";
    var opts = { armored: diti_pubkey };

  kbpgp.KeyManager.import_from_armored_pgp(opts, function(err, diti) {
    if (!err) {
      keyManager = diti;
    } else {
      console.log('Loading of OpenPGP key 0xCD42FF00 in KeyManager failed');
    }
  });
}

function encryptMsg(keyManager) {
  btnSubmit.disabled = true;

  // Use the KeyManager instance `diti` to encrypt
  if (keyManager !== undefined) {
      var opts = {
          msg:         document.getElementById('message').value,
          encrypt_for: keyManager
      };

      kbpgp.box(opts, function(err, result_string, result_buffer) {
          changeForm(result_string);
      });
  } else {
      console.log("KeyManager didn't instanciate successfully");
  }
}

function changeForm(result_string) {
  btnSubmit.disabled = false;

  if (result_string !== undefined) {
    fieldMessage.value = result_string;
    btnSubmit.innerHTML = STR_SEND;
  } else {
    console.log("Message didn't encrypt successfully");
  }
}
